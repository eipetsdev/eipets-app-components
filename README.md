## Generic components for the awesome EiPets Project!

### Usage
```javascript
import { PrimaryButton } from 'eipets-app-components';

...

<PrimaryButton text="Hello Word" />
```