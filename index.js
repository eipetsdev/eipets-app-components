import fontsConfig from './src/assets/fonts';
import AutoCompleteClass from './src/components/AutoComplete';
import AvatarBoxClass, { AvatarBoxOption as AvatarBoxOptionClass } from './src/components/AvatarBox';
import CardClass from './src/components/Card';
import CardWithActionsClass from './src/components/CardWithActions';
import CheckBoxClass from './src/components/Checkbox';
import ContentTextClass from './src/components/ContentText';
import CurrentPetHeaderAvatarClass from './src/components/CurrentPetHeaderAvatar';
import CustomSwitchClass from './src/components/CustomSwitch';
import DatePickerClass from './src/components/DatePicker';
import DefaultTextClass from './src/components/DefaultText';
import InputCounterClass from './src/components/InputCounter';
import EiPetsIconFn from './src/components/EiPetsIcon';
import FacebookButtonClass from './src/components/FacebookButton';
import FlatSelectListClass from './src/components/FlatSelectList';
import FooterActionsClass from './src/components/FooterActions';
import GoogleButtonClass from './src/components/GoogleButton';
import HeaderAvatarClass from './src/components/HeaderAvatar';
import IconActionClass from './src/components/IconAction';
import InputTextClass from './src/components/InputText';
import DetailedSelectableItemClass from './src/components/DetailedSelectableItem';
import ListDividerClass from './src/components/ListDivider';
import ListItemClass from './src/components/ListItem';
import ModalClass from './src/components/Modal';
import PickerModalSelectClass from './src/components/PickerModalSelect';
import PrimaryButtonClass from './src/components/PrimaryButton';
import RefreshBtnClass from './src/components/RefreshBtn';
import SecondaryButtonClass from './src/components/SecondaryButton';
import SectionTitleClass from './src/components/SectionTitle';
import SimpleCardClass from './src/components/SimpleCard';
import SubHeaderClass from './src/components/SubHeader';
import TaskSliderClass from './src/components/TaskSlider';
import TextareaClass from './src/components/Textarea';
import TextWithLetterSpacingClass from './src/components/TextWithLetterSpacing';
import TimePickerClass from './src/components/TimePicker';
import ToastClass from './src/components/Toast';
import ToolbarClass from './src/components/Toolbar';
import AlphabeticFlatListClass from './src/components/AlphabeticFlatList';
import WhiteSpaceClass from './src/components/WhiteSpace';
import colorsConfig from './src/config/colors';

export class AutoComplete extends AutoCompleteClass { };
export class InputText extends InputTextClass { };
export class DefaultText extends DefaultTextClass { };
export class ContentText extends ContentTextClass { };
export class RefreshBtn extends RefreshBtnClass { };

export class PrimaryButton extends PrimaryButtonClass { };
export class SecondaryButton extends SecondaryButtonClass { };
export class SubHeader extends SubHeaderClass { };
export class CustomSwitch extends CustomSwitchClass { }
export class InputCounter extends InputCounterClass { }
export class Card extends CardClass { };
export class Toast extends ToastClass { };
export class SimpleCard extends SimpleCardClass { };
export class CardWithActions extends CardWithActionsClass { };
export class FlatSelectList extends FlatSelectListClass { };
export class CheckBox extends CheckBoxClass { };
export class Toolbar extends ToolbarClass { };
export class DatePicker extends DatePickerClass { };
export class Modal extends ModalClass { };
export class HeaderAvatar extends HeaderAvatarClass { };
export class SectionTitle extends SectionTitleClass { };
export class AvatarBox extends AvatarBoxClass { };
export class AvatarBoxOption extends AvatarBoxOptionClass { };
export class PickerModalSelect extends PickerModalSelectClass { };
export class Textarea extends TextareaClass { };
export class TimePicker extends TimePickerClass { };
export class ListItem extends ListItemClass { };
export class ListDivider extends ListDividerClass { };
export class TextWithLetterSpacing extends TextWithLetterSpacingClass { };
export class FacebookButton extends FacebookButtonClass { };
export class GoogleButton extends GoogleButtonClass { };
export class DetailedSelectableItem extends DetailedSelectableItemClass { };
export class IconAction extends IconActionClass { };
export class WhiteSpace extends WhiteSpaceClass { };
export class FooterActions extends FooterActionsClass { };
export class TaskSlider extends TaskSliderClass { };
export class AlphabeticFlatList extends AlphabeticFlatListClass {}

export class CurrentPetHeaderAvatar extends CurrentPetHeaderAvatarClass { };

export function EiPetsIcon (props) {
  return EiPetsIconFn(props)
};
export const colors = colorsConfig;
export const fonts = fontsConfig;
