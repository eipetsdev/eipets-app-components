import {Platform} from 'react-native';

const family = {
  raleway: {
    ...Platform.select({
      ios: {
        fontFamily: 'Raleway'
      },
      android: {
        fontFamily: 'raleway'
      }
    })
  },
  teko: {
    ...Platform.select({
      ios: {
        fontFamily: 'Teko'
      },
      android: {
        fontFamily: 'teko'
      }
    })
  }
}

export default {
  raleway : {
    regular: {
      ...family.raleway,
      ...Platform.select({
        ios: {
          fontWeight: '500'
        },
        android: {
          fontWeight: '100'
        }
      })
    },
    bold: {
      ...family.raleway,
      fontWeight: 'bold'
    }
  },
  teko : {
    regular: {
      ...family.teko,
      fontWeight: '100'
    },
    semibold: {
      ...family.teko,
      ...Platform.select({
        ios: {
          fontWeight: '500'
        },
        android: {
          fontWeight: '100'
        }
      })
    },
    extraSemibold: {
      ...family.teko,
      ...Platform.select({
        ios: {
          fontWeight: '600'
        },
        android: {
          fontWeight: '400'
        }
      })
    },
    bold: {
      ...family.teko,
      fontWeight: 'bold'
    }
  }
};