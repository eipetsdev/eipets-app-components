import { Body, View } from 'native-base';
import React, { Component } from 'react';
import { Platform, StyleSheet, TouchableOpacity, Text } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components/native';
import fonts from '../assets/fonts';
import colors from '../config/colors';
import ContentText from './ContentText'

const Wrapper = styled.View`
  flex-direction: row;
  margin-right: ${props => props.mini ? '12px' : 0};
  margin-top: 4px;
  padding: ${props => props.mini ? 0 : (props.medium ? 0 : '12px')};
  margin-bottom: ${props => props.medium ? '12px' : 0};
`

const CustomCheckboxWrapper = styled.View`
  border-width: 2;
  border-color: ${colors.primary};
  ${Platform.select({
    ios: {
      shadowColor: 'rgba(0,0,0, .2)',
      shadowOffset: {
        height: 0,
        width: 0
      },
      shadowOpacity: 10,
      shadowRadius: 10
    },
    android: {}
  })};
  padding: 4px;
  border-radius: 8px;
  width: 36px;
  height: 36px;
`

const InnerCheckbox = styled.View`
  width: undefined;
  border-radius: 6px;
  border-color: transparent;
  height: undefined;
  flex: 1;
  background-color: ${props => props.checked ? colors.primaryDark : 'transparent'};
`

const StyledLabel = styled.Text`
  ${props => props.mini ? fonts.teko.semibold : fonts.raleway.regular};
  font-size: 18px;
  margin-left: ${props => props.mini ? '8px' : '18px'};
  color: ${colors.primary};
`

const StyledLabelDescription = styled(ContentText)`
  margin-left: ${props => props.mini ? '8px' : '18px'};
`

export default class Checkbox extends Component {
  render () {
    return (
      <Wrapper {...this.props}>
        <TouchableOpacity onPress={this.props.onPress}>
          {(this.props.mini || this.props.medium) && (
            <View style={{
              flex: 1
            }}>
              <MaterialCommunityIcons
                name={this.props.checked
                  ? 'checkbox-marked-outline'
                  : 'checkbox-blank-outline'}
                size={this.props.mini ? 26 : 36}
                color={colors.primaryDark} />
            </View>
          )}
          {(!this.props.mini && !this.props.medium) && (
            <CustomCheckboxWrapper {...this.props}>
              <InnerCheckbox {...this.props}></InnerCheckbox>
            </CustomCheckboxWrapper>
          )}
        </TouchableOpacity>
        {!!this.props.label && (
          <Body style={{ flex: 0, alignItems: 'flex-start' }}>
            <StyledLabel {...this.props}>
              {this.props.label}
            </StyledLabel>
            {!!this.props.labelDescription && (
              <StyledLabelDescription {...this.props}>{this.props.labelDescription}</StyledLabelDescription>
            )}
          </Body>
        )}
      </Wrapper>
    )
  }
}