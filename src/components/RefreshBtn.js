import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../config/colors';
import styled from 'styled-components/native';

const StyledBtnWrapper = styled.TouchableOpacity`
  border-width: 2px;
  border-color: ${colors.lightGray};
  border-radius: 100px;
  padding: 8px;
  flex: 0;
  align-self: center;
`

export default class RefreshBtn extends Component {
  render () {
    return (
      <StyledBtnWrapper onPress={this.props.onRefresh}>
        <MaterialIcon name='refresh' size={30} color={colors.primary} />
      </StyledBtnWrapper>
    )
  }
}