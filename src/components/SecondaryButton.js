import { Text } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react'
import { StyleSheet, TouchableHighlight, View } from 'react-native';

import colors from '../config/colors';

const sizes = {
  btnHeight: 55,
  smallBtnHeight: 42
};

class SecondaryButton extends Component {
  constructor(props) {
    super(props);

    this.getIcon = this.getIcon.bind(this);
  }

  getIcon () {
    return (
      <View style={styles.iconWrapper}>
        {this.props.icon}
      </View>
    )
  }

  render () {
    return (
      <View
        style={[
          this.props.style,
          styles.wrapper, {
            height: this.props.small ? sizes.smallBtnHeight : sizes.btnHeight,
            opacity: this.props.disabled ? 0.3 : 1
          }
        ]}>
        <TouchableHighlight
          onPress={(event) => !this.props.disabled && !!this.props.onPress
            ? this.props.onPress(event)
            : void (0)}
          onPressIn={() => { }}
          underlayColor='#FFF'
          activeOpacity={0.8}
          style={[
            styles.touchWrap,
            {
              height: this.props.small ? sizes.smallBtnHeight : sizes.btnHeight,
              borderColor: this.props.color || colors.primary
            }
          ]}>
          <View style={[
            styles.container,
            {
              height: this.props.small ? sizes.smallBtnHeight : sizes.btnHeight,
            }
          ]}>
            {!!this.props.icon && this.props.iconPosition !== 'right' && this.getIcon()}
            <View style={styles.button}>
              <Text style={[styles.text, {
                color: this.props.color || colors.primary
              }]}>{this.props.text}</Text>
            </View>
            {!!this.props.icon && this.props.iconPosition === 'right' && this.getIcon()}
          </View>
        </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
    borderColor: 'transparent'
  },
  container: {
    flexDirection: 'row',
    alignSelf: 'stretch'
  },
  touchWrap: {
    borderWidth: 2,
    borderRadius: 10,
    flex: 1,
    alignSelf: 'stretch'
  },
  button: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: 'transparent',
    margin: 0,
    padding: 0,
    marginLeft: 0,
    marginRight: 0,
    zIndex: 1
  },
  text: {
    fontFamily: 'Raleway',
    fontWeight: '700',
    textAlign: 'center'
  },
  iconWrapper: {
    padding: 11,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 8,
    width: 45,
    paddingRight: 8,
    borderRadius: 10
  }
});

SecondaryButton.propTypes = {
  disabled: PropTypes.bool,
  icon: PropTypes.element,
  iconPosition: PropTypes.string,
  onPress: PropTypes.func,
  text: PropTypes.string,
  buttonColor: PropTypes.string,
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number, PropTypes.array])
}

export default SecondaryButton;