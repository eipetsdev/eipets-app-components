import React, { Component } from 'react';
import { Platform, TouchableHighlight, View } from 'react-native';

import colors from '../config/colors';

export const metrics = {
  defaultRadius: 12
}

export default class Card extends Component {
  render () {
    return (
      <TouchableHighlight
        underlayColor={colors.lightBackground}
        activeOpacity={this.props.onPress ? 0.7 : 1}
        style={[styles.wrapper, this.props.wrapperStyle]}
        onPress={() => this.props.onPress && this.props.onPress()}>
        <View style={[styles.container, this.props.style]}>
          {this.props.children}
        </View>
      </TouchableHighlight>
    )
  }
}

const shadown = {
  ...Platform.select({
    ios: {
      shadowColor: 'rgba(0,0,0, .5)',
      shadowOffset: {
        height: 0,
        width: 0
      },
      shadowOpacity: 0.8,
      shadowRadius: 5
    },
    android: {
      borderRadius: metrics.defaultRadius,
      elevation: 4,
      marginVertical: 6,
      marginHorizontal: 4,
      marginTop: 1
    }
  })
};

const styles = {
  wrapper: {
    borderRadius: metrics.defaultRadius - 2,
    ...shadown
  },
  container: {
    flex: 1,
    minHeight: 50,
    backgroundColor: '#FFF',
    borderRadius: metrics.defaultRadius
  }
};