import Fuse from 'fuse.js';
import _ from 'lodash';
import { Icon, Input, Item } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Keyboard, StyleSheet, Text, TouchableHighlight, View, Platform } from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'

import fonts from '../assets/fonts';
import colors from '../config/colors';
import styled from 'styled-components/native';
import InputText from './InputText';

const defaultTake = 4;
const searchOptions = {
  shouldSort: true,
  includeScore: true,
  includeMatches: true,
  threshold: 0.6,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 0
};

const ItemWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  border-width: 1px;
  padding: 12px;
  padding-top: 7px;
  padding-bottom: 7px;
  margin-top: -1px;
  margin-left: ${Platform.select({ ios: 0, android: '2px' })};
  border-color: ${colors.extraLightGray};
  background-color: #FFFFFF;
  border-bottom-right-radius: ${props => props.isLastIndex ? '12px' : '0px'};
  border-bottom-left-radius: ${props => props.isLastIndex ? '12px' : '0px'};
`

function ItemTemplate ({ label = '', isLastIndex = false, icon, onSelect } = props) {
  return (
    <TouchableHighlight onPress={() => onSelect && onSelect(label)}>
      <ItemWrapper isLastIndex={isLastIndex}>
        {icon && (<icon />)}
        {!icon && (<MaterialIcon
          style={{ marginRight: 12 }}
          name={'search'}
          color={colors.lightGray} size={22} />)}
        <Text style={{
          ...fonts.raleway.regular,
          color: colors.lightGray
        }}>{label.length > 30 ? `${label.substr(0, 30)}...` : label}</Text>
      </ItemWrapper>
    </TouchableHighlight>
  )
}

class AutoComplete extends Component {
  constructor(render) {
    super(render);

    this.state = {
      search: this.props.value,
      data: [],
      focused: false
    }

    this.inputRef;
    this.onChangeText = this.onChangeText.bind(this);
    this.onPick = this.onPick.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onFoucs = this.onFoucs.bind(this);
    this.fuse = new Fuse(this.props.source, searchOptions);
  }

  componentWillReceiveProps (newProps) {
    this.fuse = new Fuse(newProps.source, searchOptions);

    if (this.props.value !== newProps.value) {
      this.setState({
        search: newProps.value
      })
    }

    if (newProps.loading != this.props.loading) {
      this.onChangeText(newProps.value);
    }
  }

  onChangeText (value) {
    if (this.props.keyboardType == 'numeric') {
      value = value.replace(/[^0-9]/g, '');
    }

    let source = this.props.source || [];
    let filteredValues = this.fuse.search(value);
    this.setState({
      search: value,
      data: _.take(filteredValues, this.props.take || defaultTake)
    });

    this.props.onChangeText && this.props.onChangeText(value);
  }

  onPick (value) {
    this.setState({
      search: value
    });
    Keyboard.dismiss();
    this.props.onPick && this.props.onPick(value);
    this.props.onChangeText && this.props.onChangeText(value);
  }

  onBlur (externalOnBlur) {
    return () => {
      this.setState({ focused: false })

      if (this.props.forceSelectOnBlur && this.props.source.indexOf(this.state.search) === -1) {
        let match = this.state.data[0];
        let value = match && match.matches[0].value || ''
        this.onPick(value);
      }

      externalOnBlur && externalOnBlur()
    }
  }

  onFoucs () {

    if (this.props.forceSelectOnBlur) {
      this.onChangeText('')
    }
    this.setState({ focused: true })

    this.props.onFocus ? this.props.onFocus() : null
  }

  render () {
    return (
      <View style={{
        position: 'relative',
        zIndex: 10,
        minHeight: this.state.focused && !this.props.disableAutoHeight ? (55 + (38 * defaultTake)) : 0
      }}>
        <InputText
          {...this.props}
          style={{
            ...this.props.style,
            marginLeft: Platform.select({
              ios: -2,
              android: 0
            }),
            borderBottomRightRadius: this.state.focused && this.state.data.length > 0 ? 0 : 12,
            borderBottomLeftRadius: this.state.focused && this.state.data.length > 0 ? 0 : 12,
          }}
          value={this.state.search}
          onFocus={this.onFoucs}
          onBlur={this.onBlur(this.props.onBlur)}
          onChangeText={this.onChangeText}
        />
        {this.state.focused && (
          <View>
            {!this.props.loading && this.state.data.map((item, index) => (
              <ItemTemplate
                key={index}
                label={item.matches[0].value}
                onSelect={(value) => {
                  this.onPick(value);
                  Keyboard.dismiss();
                }}
                isLastIndex={this.state.data.length === (index + 1)} />
            ))}
            {this.state.search.length > 0 && this.props.loading && (
              <ItemTemplate label={'Carregando sugestões...'} isLastIndex={true} />
            )}
          </View>
        )}
      </View>
    )
  }
}

export default AutoComplete;