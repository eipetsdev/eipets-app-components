import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { StyleSheet, Text, TouchableHighlight, TouchableWithoutFeedback, View } from 'react-native';
import Interactable from 'react-native-interactable';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import fonts from '../assets/fonts'
import colors from '../config/colors';
import Card from './Card';
import EiPetsIcon from './EiPetsIcon';
import IconAction from './IconAction';
import TextWithLetterSpacing from './TextWithLetterSpacing';

export const compactMetrics = {
  defaultPadding: 18
}

export const metrics = {
  defaultPadding: 32,
  sizePerAction: 62
};

function getDefaultPadding (props) {
  return props.compactPadding ? compactMetrics.defaultPadding : metrics.defaultPadding
}

function IconBox (props) {
  return !!props.icon && (
    <View
      style={{
        flex: 0,
        minWidth: 65,
        minHeight: 65,
        maxHeight: 180,
        backgroundColor: props.iconBg || colors.primary,
        borderTopRightRadius: props.showIconOnTop ? 0 : 8,
        borderBottomRightRadius: 8,
        borderBottomLeftRadius: props.showIconOnTop ? 8 : 0,
        marginLeft: props.showIconOnTop ? (props.fitIcon ? 'auto' : 0) : getDefaultPadding(props) * -1,
        marginRight: props.showIconOnTop ? (props.fitIcon ? 'auto' : 0) : 0,
        alignItems: 'center',
        marginTop: props.showIconOnTop ? getDefaultPadding(props) * -1 : 0,
        marginBottom: props.showIconOnTop ? 12 : 0,
        justifyContent: 'center'
      }}>
      {(props.icon)}
    </View>
  )
}

export default class SimpleCard extends Component {
  constructor(props) {
    super(props)

    this.interactable;
  }


  render () {
    let totalActions = (!!this.props.onRemove ? 1 : 0)
      + (!!this.props.onDone ? 1 : 0)
      + (!!this.props.onUndone ? 1 : 0)
      + (!!this.props.onRequest ? 1 : 0)
      + (!!this.props.onEdit ? 1 : 0);
    let dragEnabledIsDefined = typeof this.props.dragEnabled != 'undefined'

    return (

      <View>
        <Interactable.View
          ref={(ref) => this.interactable = ref}
          horizontalOnly={true}
          style={this.props.style}
          dragEnabled={(dragEnabledIsDefined ? this.props.dragEnabled : totalActions > 0)}
          dragToss={0.01}
          snapPoints={[
            {
              x: 0
            }, {
              x: ((totalActions * metrics.sizePerAction) + 30) * -1
            }
          ]}>
          <Card
            onPress={this.props.onPress}
            wrapperStyle={this.props.wrapperStyle}
            style={[{
              padding: getDefaultPadding(this.props),
              paddingTop: getDefaultPadding(this.props),
              paddingBottom: getDefaultPadding(this.props)
            }, this.props.cardStyle]}>
            {this.props.showIconOnTop && (
              <IconBox {...this.props} />
            )}
            {!!this.props.title && this.props.showTitleOnTop && (
              <View style={{ marginBottom: 24 }}>
                <Text
                  style={{
                    ...fonts.teko.semibold,
                    color: colors.primary,
                    fontSize: 26,
                    textAlign: 'center',
                    ...this.props.titleStyles
                  }}>{this.props.title.toUpperCase()}</Text>
              </View>
            )}
            <View style={{
              flexDirection: 'row',
              alignItems: 'stretch',
              alignContent: 'stretch'
            }}>
              {!this.props.showIconOnTop && (
                <IconBox {...this.props} />
              )}
              <View
                style={{
                  flex: 1,
                  paddingLeft: this.props.showIconOnTop ? 0 : (this.props.icon ? compactMetrics.defaultPadding : 0),
                  justifyContent: 'center'
                }}>
                {!!this.props.time && (
                  <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginBottom: 6
                  }}>
                    <MaterialIcons name="schedule" size={12} color={colors.lightTextColor} />
                    <Text style={{
                      ...fonts.raleway.regular,
                      fontSize: 11,
                      paddingLeft: 4,
                      color: this.props.timeColor || colors.lightTextColor
                    }}>{this.props.time}</Text>
                  </View>
                )}
                {!!this.props.title && !this.props.showTitleOnTop && (
                  <Text
                    style={{
                      ...fonts.teko.semibold,
                      color: colors.primary,
                      fontSize: 17,
                      ...this.props.titleStyles
                    }}>{this.props.title.toUpperCase()}</Text>
                )}
                {!!this.props.subTitle && (
                  <Text
                    style={{
                      ...fonts.teko.semibold,
                      fontSize: 15,
                      lineHeight: 20,
                      color: colors.textColor,
                      ...this.props.subTitleStyles
                    }}>{this.props.subTitle.toUpperCase()}</Text>
                )}
                {!!this.props.text && (
                  <Text
                    style={{
                      ...fonts.raleway.regular,
                      fontSize: 14,
                      color: colors.textColor
                    }}>{this.props.text}</Text>
                )}
                {!!this.props.customText && (this.props.customText)}
                {!!this.props.footerText && (
                  <Text
                    style={{
                      ...fonts.raleway.regular,
                      fontSize: 13,
                      color: colors.lightTextColor
                    }}>{this.props.footerText}</Text>
                )}
                {!!this.props.footer && (
                  <TouchableWithoutFeedback onPressIn={() => { }}>
                    <View
                      style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingTop: compactMetrics.defaultPadding,
                        paddingBottom: compactMetrics.defaultPadding,
                        marginBottom: compactMetrics.defaultPadding * -1
                      }}>
                      {(this.props.footer)}
                    </View>
                  </TouchableWithoutFeedback>
                )}
              </View>
            </View>
          </Card>
        </Interactable.View>
        <View
          style={{
            position: 'absolute',
            top: 0,
            bottom: getDefaultPadding(this.props),
            right: 8,
            zIndex: -1,
            width: (totalActions * metrics.sizePerAction),
            flex: 1,
            flexDirection: 'row',
            justifyContent: totalActions > 1 ? 'space-around' : 'center',
            alignContent: 'center',
            alignItems: 'center'
          }}>
          {!!this.props.onEdit && (<IconAction
            onPress={() => {
              this.interactable.snapTo({ index: 0 })
              !!this.props.onEdit && this.props.onEdit()
            }}
            color={colors.primary}
            label={'Editar'} />)}
          {!!this.props.onRemove && (<IconAction
            onPress={() => {
              this.interactable.snapTo({ index: 0 })
              !!this.props.onRemove && this.props.onRemove()
            }}
            color={colors.primary}
            label={'Excluir'} />)}
          {!!this.props.onRequest && (<IconAction
            onPress={() => {
              this.interactable.snapTo({ index: 0 })
              !!this.props.onRequest && this.props.onRequest()
            }}
            color={colors.primary}
            label={'Solicitar'} />)}
        </View>
      </View>
    )
  }
}

export const simpleCardStyles = {
  customTextLabel: {
    ...fonts.raleway.bold,
    fontSize: 13
  },
  customTextValue: {
    ...fonts.raleway.regular,
    color: colors.textColor,
    fontSize: 13
  }
};