import React, { Component } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'

import fonts from '../assets/fonts';
import colors from '../config/colors';

export default class ListDivider extends Component {
  render () {
    let text = this.props.text || '';
    return (
      <View
        style={{
          paddingLeft: 16,
          paddingTop: 9,
          paddingBottom: 9,
          backgroundColor: colors.extraLightGray,
          flexDirection: 'row',
        }}>
        <View style={{
          flex: 1,
          justifyContent: 'center'
        }}>
          <Text
            style={{
              fontSize: 14,
              ...fonts.raleway.bold,
              color: colors.primaryDark,
            }}>{text.toUpperCase()}</Text>
        </View>
        {!!this.props.icon && (
          <View style={{ paddingRight: 12 }}>
            <TouchableOpacity onPress={this.props.icon.onPress}>
              {this.props.icon}
            </TouchableOpacity>
          </View>
        )}
      </View>
    )
  }
}
