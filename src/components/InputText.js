import { Icon, Input, Item } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'

import colors from '../config/colors';
import styled from 'styled-components/native';


const IconWrapper = styled.View`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  padding-left: 12px;
  padding-right: 12px;
  z-index: 10;
`

const StyledInput = styled(Input)`
  border-radius: 12px;
  background-color: ${colors.white};
  border-width: ${props => props.noBorder ? 0 : '2px'};
  height: ${props => props.small ? '40px' : '55px'};
  padding-right: ${props => props.icon && props.iconPosition !== 'left' ? '46px' : '6px'};
  padding-left: ${props => props.icon && props.iconPosition === 'left' ? '46px' : '14px'};
  border-color: ${colors.primary};
`

const Wrapper = styled(Item)`
  position: relative;
  border-radius: 0px;
  border-color: transparent;
  justify-content: center;
`

class InputText extends Component {
  constructor(render) {
    super(render);

    this.getInputStyle = this.getInputStyle.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  getInputStyle () {
    return {

    };
  }

  onChangeText (value) {
    if (!this.props.onChangeText) {
      return;
    }

    if (this.props.keyboardType == 'numeric') {
      value = value.replace(/[^0-9.,]/g, '');
    }

    return this.props.onChangeText(value);
  }

  render () {
    return (
      <Wrapper rounded style={{
        ...this.props.containerStyle
      }}>
        {this.props.icon && this.props.iconPosition == 'left' && (
          <IconWrapper>
            {this.props.icon}
          </IconWrapper>
        )}
        <StyledInput
          {...this.props}
          onChangeText={this.onChangeText} />
        {this.props.icon && this.props.iconPosition != 'left' && (
          <IconWrapper>
            {this.props.icon}
          </IconWrapper>
        )}
      </Wrapper>
    )
  }
}

export default InputText;