import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';

import fonts from '../assets/fonts';
import colors from '../config/colors';
import Card from './Card';
import EiPetsIcon from './EiPetsIcon';

class CardWithActions extends Component {
  render() {
    return (
      <View>
        <Card 
            icon={this.props.icon}
            title={this.props.title}
            content={this.props.content}
        />
        <View style={styles.container}>
            <TouchableOpacity style={styles.action} onPress={this.props.onPressCancel}>
                <EiPetsIcon style={styles.icon} name="health" size={40} color={colors.icon} />
                <Text style={styles.label}> NÃO FEITO </Text>                
            </TouchableOpacity>
            <TouchableOpacity style={styles.action} onPress={this.props.onPressConfirm}>
                <EiPetsIcon style={styles.icon} name="health" size={40} color={colors.icon} />
                <Text style={styles.label}> FEITO </Text>                            
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

CardWithActions.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    content: PropTypes.array,
    onPressCancel: PropTypes.func.isRequired,
    onPressConfirm: PropTypes.func.isRequired,
  }

const styles = StyleSheet.create({
    container: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#464c56',
        marginTop: -5,
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        backgroundColor: '#B8107B',
        position: 'relative'
    },
    icon: {
        textAlign: 'center',
        margin: 5,
    },
    label: {
        ...fonts.raleway.bold,
        fontWeight: 'bold',
        color: 'white'
    },
    action: {
        flexDirection: 'column',
        alignItems: 'center',
        margin: '5%'
    }
})

export default CardWithActions;