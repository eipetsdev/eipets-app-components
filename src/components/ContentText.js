import React, { Component } from 'react';
import styled from 'styled-components/native';
import fonts from '../assets/fonts';
import colors from '../config/colors';

const StyledText = styled.Text`
  ${fonts.raleway.regular};
  color: ${colors.textColor};
  text-align: ${props => props.center ? 'center' : 'left'};
  margin: 0;
  padding: 0;
  font-size: 15px;
  line-height: 22px;
  margin-bottom: ${props => props.noMargin ? 0 : '18px'};
`

export default class ContentText extends Component {
  render () {
    return (
      <StyledText {...this.props}>{this.props.children}</StyledText>
    )
  }
}
