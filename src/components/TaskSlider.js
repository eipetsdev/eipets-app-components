import React, { Component } from 'react';
import { View, Text, TouchableOpacity, InteractionManager, Slider } from 'react-native'
import { colors } from '../..';
import PubSub from 'pubsub-js';

export default class TaskSlider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.value || 0,
      scale: this.getScale() || []
    }

    this.onValueChange = this.onValueChange.bind(this);
    this.getScale = this.getScale.bind(this);
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.value != this.state.value && nextProps.value != this.props.value) {
      this.setState({ value: nextProps.value })
    }
  }

  onValueChange (value) {
    if (value - this.state.value > 1) {
      value = Math.round(value)
    } else if (value > this.state.value) {
      value = Math.ceil(value)
    } else {
      value = Math.floor(value)
    }

    this.props.onValueChange ? this.props.onValueChange(value) : void (0)
  }

  getScale () {
    let times = this.props.maximumValue

    if (times <= 1)
      return ['Não feito', 'Feito']

    let scale = []
    for (let index = 0; index < (times + 1); index++) {
      scale.push(index)
    }
    return scale
  }

  render () {
    return (
      <View style={{
        width: '100%'
      }}>
        <Slider
          verticalScrollingDisabled={true}
          style={{
            width: '100%'
          }}
          ref={ref => (this.slider = ref)}
          value={this.state.value}
          minimumValue={0}
          maximumValue={this.props.maximumValue}
          thumbTintColor={colors.primary}
          minimumTrackTintColor={colors.primaryDark}
          maximumTrackTintColor={colors.lightGray}
          onSlidingComplete={(value) => this.onValueChange(value)}
        />
        <View style={{
          flexDirection: 'row',
          justifyContent: 'space-between'
        }}>
          {this.state.scale.map((label, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => this.onValueChange(index)}>
              <Text style={{
                marginBottom: -12,
                padding: 12,
                paddingLeft: 3,
                paddingRight: 3,
                marginLeft: index == 0 && isNaN(label) ? -3 : 0,
                marginRight: this.state.scale.length == index + 1 && isNaN(label) ? -3 : 0
              }}>{label}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    )
  }
};
