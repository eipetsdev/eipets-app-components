import React, { Component } from 'react'
import { Platform, View } from 'react-native'
import RNDatepicker from 'react-native-datepicker';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'

import fonts from '../assets/fonts';
import colors from '../config/colors';

export default class TimePicker extends Component {
  render () {
    let componentHeight = this.props.small ? 41 : 55

    return (
      <View style={[styles.wrapper, {
        height: componentHeight,
        width: this.props.small ? 110 : undefined
      }, this.props.style]}>
        <RNDatepicker
          {...this.props}
          is24Hour={true}
          style={styles.datePickerWrapper}
          placeholder={this.props.placeholder || 'Selecionar'}
          mode='time'
          confirmBtnText='Confirmar'
          cancelBtnText='Cancelar'
          iconComponent={(
            <View
              style={{
                ...Platform.select({
                  ios: {
                    borderTopRightRadius: 8,
                    borderBottomRightRadius: 8
                  },
                  android: {
                    borderWidth: 1,
                    borderColor: 'transparent',
                    borderTopRightRadius: 12,
                    borderBottomRightRadius: 12,
                  }
                }),
                width: this.props.small ? 35 : 55,
                height: componentHeight,
                right: -2,
                backgroundColor: colors.primaryDark,
                alignItems: 'center',
                justifyContent: 'center'
              }}>
              <MaterialCommunityIcon name='clock' color={'#FFF'} size={this.props.small ? 22 : 28} />
            </View>
          )}
          customStyles={customStyles(componentHeight)} />
      </View>
    )
  }
}

const customStyles = (componentHeight) => ({
  dateTouchBody: {
    height: (componentHeight - 3)
  },
  dateInput: {
    width: undefined,
    height: undefined,
    borderWidth: 0,
    alignItems: 'flex-start',
    height: componentHeight,
    alignSelf: 'stretch',
    padding: 12
  },
  dateText: {
    ...fonts.raleway.regular,
    fontSize: 17,
    lineHeight: 15
  },
  placeholderText: {
    ...fonts.raleway.regular,
    color: colors.placeholder,
    fontSize: 17,
    lineHeight: 15
  },
  btnTextConfirm: {
    color: colors.primary
  }
});

const styles = {
  wrapper: {
    flexDirection: 'row',
    flex: 0,
    width: undefined,
    borderRadius: 12,
    borderWidth: 2,
    backgroundColor: colors.white,
    borderColor: colors.primary
  },
  datePickerWrapper: {
    flex: 1
  }
};