import { Container, Text } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react'
import { Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';

import fonts from '../assets/fonts';
import colors from '../config/colors';

const shadown = {
  ...Platform.select({
    ios: {
      shadowColor: 'rgba(0,0,0, .3)',
      shadowOffset: {
        height: 2,
        width: 0
      },
      shadowOpacity: 10,
      shadowRadius: 2
    },
    android: {
      borderRadius: 2,
      elevation: 3
    }
  })
}

export default class FacebookButton extends Component {
  render () {
    return (
      <Container
        style={{
          flex: 0,
          height: 55,
          width: '100%',
          ...this.props.style
        }}>
        <TouchableOpacity
          onPress={this.props.onPress}
          onPressIn={() => { }}
          underlayColor={colors.lightBackground}
          activeOpacity={0.7}
          style={{
            backgroundColor: '#FFF',
            flex: 1,
            borderRadius: 8,
            flexDirection: 'row',
            alignItems: 'stretch',
            ...shadown
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center'
            }}>
            <Entypo
              style={{
                paddingLeft: 14,
                paddingRight: 14,
                flex: 0
              }}
              name="facebook"
              color={'#4B66B5'}
              size={28} />
            <Text
              style={{
                flex: 1,
                color: colors.textColor
              }}>ENTRAR COM O FACEBOOK</Text>
          </View>
        </TouchableOpacity>
      </Container>
    )
  }
}
