import React, { Component } from 'react';
import { TouchableHighlight, View, InteractionManager } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import fonts from '../assets/fonts';
import colors from '../config/colors';
import EiPetsIcon from './EiPetsIcon';
import TextWithLetterSpacing from './TextWithLetterSpacing';

export default class IconAction extends Component {
  render () {
    return (
      <View style={[{
        alignItems: 'center'
      }, this.props.style]}>
        <TouchableHighlight
          style={{
            width: 42,
            height: 42,
            marginBottom: 12
          }}
          underlayColor={this.props.underlayColor || colors.lightBackground}
          activeOpacity={0.5}
          onPress={() => {
            !!this.props.onPress && InteractionManager.runAfterInteractions(() => {
              this.props.onPress()
            })
          }}>
          <View
            style={{
              width: 42,
              height: 42,
              borderRadius: 8,
              borderWidth: 2,
              borderColor: this.props.color,
              backgroundColor: this.props.isActive ? this.props.color : 'transparent',
              alignItems: 'center',
              justifyContent: 'center'
            }}>
            {!!this.props.icon && (this.props.icon)}
            {!this.props.icon && (
              <View>
                {this.props.label == 'Feito' && (<EiPetsIcon name={'heart'} size={this.props.isActive ? 26 : 22} color={this.props.isActive ? this.props.activeColor : this.props.color} />)}
                {this.props.label == 'Editar' && (<MaterialCommunityIcons name={'pencil'} size={this.props.isActive ? 26 : 22} color={this.props.isActive ? this.props.activeColor : this.props.color} />)}
                {this.props.label == 'Excluir' && (<EiPetsIcon name={'broken-hearth'} size={this.props.isActive ? 26 : 22} color={this.props.isActive ? this.props.activeColor : this.props.color} />)}
                {this.props.label == 'Solicitar' && (<EiPetsIcon name={'business'} size={this.props.isActive ? 26 : 22} color={this.props.isActive ? this.props.activeColor : this.props.color} />)}
              </View>
            )}
          </View>
        </TouchableHighlight>
        {this.props.label.split(' ').map((word, index) => (
          <TextWithLetterSpacing
            key={index}
            textStyle={{
              ...fonts.teko.semibold,
              fontSize: 14,
              color: this.props.color
            }}
            spacing={1.5}>
            {word.toUpperCase()}
          </TextWithLetterSpacing>
        ))}
      </View>
    )
  }
}
