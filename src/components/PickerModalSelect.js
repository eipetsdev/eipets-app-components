import React, { Component } from 'react'
import { Text, View } from 'react-native';
import ModalSelector from 'react-native-modal-selector'
import FeatherIcon from 'react-native-vector-icons/Feather'

import fonts from '../assets/fonts';
import colors from '../config/colors';

export default class PickerModalSelect extends Component {
  render () {
    return (
      <ModalSelector
        style={[{ ...this.props.style }, {
          marginTop: 2
        }]}
        data={this.props.data}
        animationType={'fade'}
        cancelText={this.props.cancelText || 'fechar'}
        overlayStyle={{
          flex: 1,
          padding: '5%',
          justifyContent: 'center',
          backgroundColor: 'rgba(79, 4, 70, 0.70)'
        }}
        optionContainerStyle={{
          backgroundColor: '#FFF',
          borderRadius: 12,
          borderBottomRightRadius: 0,
          borderBottomLeftRadius: 0,
          marginBottom: 0
        }}
        cancelStyle={{
          padding: 12,
          backgroundColor: colors.primaryDark,
          borderBottomLeftRadius: 12,
          borderBottomRightRadius: 12,
          borderTopRightRadius: 0,
          borderTopLeftRadius: 0
        }}
        cancelTextStyle={{
          ...fonts.raleway.regular,
          fontSize: 22,
          color: '#FFF'
        }}
        optionStyle={{
          borderWidth: 0,
          borderColor: colors.primary
        }}
        optionTextStyle={{
          ...fonts.raleway.regular,
          color: colors.primary,
          fontSize: 16,
          paddingTop: 8,
          paddingBottom: 8
        }}
        sectionTextStyle={{
          ...fonts.raleway.regular,
          fontSize: 26,
          color: colors.primaryDark
        }}
        onChange={(option) => {
          if (!this.props.onChange)
            return;

          this.props.onChange(option);
        }}>
        <View
          onPress={() => { }}
          underlayColor={'#FFF'}
          activeOpacity={0.9}
          style={{
            flex: 0,
            height: this.props.small ? 40 : 55,
            borderRadius: 12,
            backgroundColor: colors.white,
            borderWidth: this.props.noBorder ? 0 : 2,
            borderColor: colors.primary,
            flexDirection: 'row'
          }}>
          <View
            style={{
              flex: 1,
              height: '100%',
              alignSelf: 'stretch',
              padding: 16,
              justifyContent: 'center'
            }}>
            {!this.props.value && (
              <Text
                numberOfLines={1}
                style={{
                  ...fonts.raleway.regular,
                  color: colors.placeholder,
                  fontSize: 17
                }}>{this.props.placeholder}</Text>
            )}
            {!!this.props.value && (
              <Text
                numberOfLines={1}
                style={{
                  ...fonts.raleway.regular,
                  color: colors.textColor,
                  fontSize: 17
                }}>{this.props.value}</Text>
            )}
          </View>
          <View
            style={{
              width: this.props.small ? 40 : 55,
              height: this.props.small ? 40 : 55,
              backgroundColor: colors.primaryDark,
              top: this.props.noBorder ? 0 : 2,
              right: this.props.noBorder ? 0 : -2,
              alignSelf: 'flex-end',
              borderTopRightRadius: 8,
              borderBottomRightRadius: 8,
              alignItems: 'center',
              justifyContent: 'center'
            }}>
            <FeatherIcon name='chevron-down' color={'#FFF'} size={28} />
          </View>
        </View>
      </ModalSelector>
    )
  }
}
