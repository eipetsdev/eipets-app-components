import _ from 'lodash';
import React, { Component } from 'react';
import brStates from '../../../localization/br-states';
import colors from '../config/colors';
import AutoComplete from './AutoComplete';
import EiPetsIcon from './EiPetsIcon';
import { AsyncStorage, Text } from 'react-native';
import styled from 'styled-components/native';

const PLACE_PICKER_STORE_KEY = 'PLACE_PICKER_STORE_KEY'

const Wrapper = styled.View`
  position: relative;
`

export default class CityPicker extends Component {
  constructor(props) {
    super(props);

    let uf = this.props.ufPriority || brStates[0].abbrev;

    this.currentUf = brStates.find(state => state.abbrev === uf);
    this.currentUfCities = _.flatten(this.currentUf.cities.map(city => `${city}, ${this.currentUf.abbrev}`))
    this.othersUf = brStates.filter(state => state.abbrev !== uf)
    this.othersUfsCities = _.flatten(this.othersUf.map(state => state.cities.map(city => `${city}, ${state.abbrev}`)))
    this.placesSource = this.currentUfCities.concat(this.othersUfsCities);

    this.state = {
      place: this.props.value || '',
      placesSource: this.placesSource
    }

    this.onPick = this.onPick.bind(this);
    this.bindStoredPlaceSelection = this.bindStoredPlaceSelection.bind(this);
  }

  componentDidMount () {
    if (this.props.storeSelection && !this.props.value) {
      this.bindStoredPlaceSelection()
    }
  }

  componentWillReceiveProps (nextProps) {
    if(nextProps.value != this.state.place) {
      this.setState({
        ...this.state,
        place: nextProps.value
      })
    }
  }

  async bindStoredPlaceSelection () {
    let place = await AsyncStorage.getItem(PLACE_PICKER_STORE_KEY);

    if (!place)
      return;

    this.onPick(place);
  }

  storePlaceSelection (place) {
    AsyncStorage.setItem(PLACE_PICKER_STORE_KEY, place);
  }

  onPick (value) {
    this.setState({
      place: value
    })

    if (this.props.storeSelection) {
      this.storePlaceSelection(value);
    }

    this.props.onPick && this.props.onPick(value)
  }
  render () {
    return (
      <Wrapper>
        <AutoComplete
          {...this.props}
          small
          noBorder
          forceSelectOnBlur
          source={this.state.placesSource}
          autoCorrect={false}
          loading={false}
          value={this.state.place}
          disableAutoHeight
          iconPosition={'left'}
          icon={(<EiPetsIcon name={'place'} size={24} color={colors.primaryDark} />)}
          placeholder="Localização"
          onPick={this.onPick}
        />
      </Wrapper>
    )
  }
}
