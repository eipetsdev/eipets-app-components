import React, { Component } from 'react'
import { Platform, View } from 'react-native'
import RNDatepicker from 'react-native-datepicker';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'

import fonts from '../assets/fonts';
import colors from '../config/colors';

export default class DatePicker extends Component {
  render() {
    return (
      <View style={[this.props.style, styles.wrapper]}>
        <RNDatepicker
          {...this.props}
          style={styles.datePickerWrapper}
          placeholder={this.props.placeholder || 'Selecionar'}
          format={this.props.format || 'DD/MM/YYYY'}
          confirmBtnText='Confirmar'
          cancelBtnText='Cancelar'
          iconComponent={(
            <View
              style={{
                ...Platform.select({
                  ios: {
                    borderTopRightRadius: 8,
                    borderBottomRightRadius: 8
                  },
                  android: {
                    borderWidth: 1,
                    borderColor: 'transparent',
                    borderTopRightRadius: 12,
                    borderBottomRightRadius: 12,
                  }
                }),
                width: 55,
                height: 55,
                right: -2,
                backgroundColor: colors.primaryDark,
                alignItems: 'center',
                justifyContent: 'center'
              }}>
              <MaterialCommunityIcon name='calendar' color={'#FFF'} size={28} />
            </View>
          )}
          customStyles={customStyles} />
      </View>
    )
  }
}

const customStyles = {
  dateTouchBody: {
    height: 52
  },
  dateInput: {
    width: undefined,
    height: undefined,
    borderWidth: 0,
    alignItems: 'flex-start',
    height: 55,
    alignSelf: 'stretch',
    padding: 16
  },
  dateText: {
    ...fonts.raleway.regular,
    fontSize: 17
  },
  placeholderText: {
    ...fonts.raleway.regular,
    color: colors.placeholder,
    fontSize: 17
  },
  btnTextConfirm: {
    color: colors.primary
  }
};

const styles = {
  wrapper: {
    flexDirection: 'row',
    flex: 0,
    width: undefined,
    height: 55,
    borderRadius: 12,
    borderWidth: 2,
    backgroundColor: colors.white,
    borderColor: colors.primary
  },
  datePickerWrapper: {
    flex: 1
  }
};