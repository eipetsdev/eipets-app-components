import React, { Component } from 'react'
import styled from 'styled-components/native';
import fonts from '../assets/fonts';
import colors from '../config/colors';

const StyledText = styled.Text`
  ${fonts.raleway.regular};
  font-size: 18px;
  text-align: center;
  color: ${colors.lightGray};
  margin-bottom: 12px;
`

export default class DefaultText extends Component {
  render () {
    return (
      <StyledText {...this.props}>{this.props.children}</StyledText>
    )
  }
}
