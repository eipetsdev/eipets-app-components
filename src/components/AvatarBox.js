import { Container } from 'native-base';
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import { CachedImage } from 'react-native-cached-image';
import fonts from '../assets/fonts';
import colors from '../config/colors';

export class AvatarBoxOption extends Component {
  render () {
    return (
      <View style={{
        flexDirection: 'row',
        height: 28,
        alignContent: 'center',
        alignItems: 'center'
      }}>
        <this.props.icon />
        <Text style={{
          ...fonts.teko.semibold,
          fontSize: 16,
          color: '#FFF',
          marginLeft: 6
        }}>{this.props.label.toUpperCase()}</Text>
      </View>
    )
  }
}

export default class AvatarBox extends Component {
  render () {
    return (
      <View style={[
        styles.wrapper,
        {
          height: this.props.avatarOption ? 138 : 110
        },
        this.props.style]}>
        <Container style={[styles.avatarBox, {
          borderBottomRightRadius: this.props.avatarOption ? 0 : 8,
          borderBottomLeftRadius: this.props.avatarOption ? 0 : 8
        }]}>
          {!!this.props.avatar && (
            <TouchableHighlight
              underlayColor={colors.lightBackground}
              activeOpacity={0.9}
              onPress={() => { this.props.onAvatarPress && this.props.onAvatarPress() }}
              style={{
                flex: 1
              }}>
              <View style={{ flex: 1 }}>
                {this.props.avatar.uri && (
                  <CachedImage source={this.props.avatar} style={styles.avatar} resizeMode={"contain"} />
                )}
                {!this.props.avatar.uri && (
                  <CachedImage source={this.props.avatar} style={styles.avatar} resizeMode={"contain"} />
                )}
              </View>
            </TouchableHighlight>
          )}
        </Container>
        {this.props.avatarOption && (
          <View style={styles.footer}>
            <TouchableHighlight
              style={styles.avatarOptionWrapper}
              underlayColor={colors.primaryDark}
              activeOpacity={0.4}
              onPress={() => {
                this.props.onAvatarOptionPress && this.props.onAvatarOptionPress()
              }}>
              <View style={styles.avatarOption}>
                <this.props.avatarOption />
              </View>
            </TouchableHighlight>
          </View>
        )}
      </View>
    )
  }
}

const shadown = {
  ...Platform.select({
    ios: {
      shadowColor: 'rgba(0,0,0, .3)',
      shadowOffset: {
        height: 0,
        width: 0
      },
      shadowOpacity: 10,
      shadowRadius: 10
    },
    android: {
      borderRadius: 10,
      elevation: 5
    }
  })
};

const styles = StyleSheet.create({
  wrapper: {
    width: 110,
    alignSelf: 'center',
  },
  footer: {
    backgroundColor: colors.primaryDark,
    height: 28,
    alignContent: 'center',
    justifyContent: 'center',
    borderBottomRightRadius: 8,
    borderBottomLeftRadius: 8
  },
  avatarOptionWrapper: {
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    display: 'flex',
    flexDirection: 'row',
    zIndex: 2
  },
  avatarOption: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatarBox: {
    ...shadown,
    padding: 8,
    backgroundColor: colors.lightBackground,
    borderRadius: 8,
    borderWidth: 0,
    borderColor: 'transparent'
  },
  avatar: {
    width: undefined,
    height: undefined,
    borderRadius: 110 / 2 - 8,
    flex: 1
  }
});