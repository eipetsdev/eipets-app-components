import React, { Component } from 'react'
import { Modal as ReactModal, View, ScrollView, TouchableOpacity } from 'react-native'
import styled from 'styled-components/native';
import colors from '../config/colors';
import fonts from '../assets/fonts';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { Content } from 'native-base';
import Toast from './Toast';
import SpinnerLoader from '../../../shared-components/SpinnerLoader';

const Overlay = styled.View`
  flex: 1;
  width: 100%;
  height: 100%;
  display: flex;
  background-color: rgba(79, 4, 70, 0.70);
  padding: 18px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`

const Container = styled.View`
  background-color: ${colors.white};
  border-radius: 12px;
  padding: 18px;
  padding-bottom: 0;
`

const Body = styled.View`
  border-top-width: 1px;
  border-top-color: ${colors.lightGray};
  padding-top: 24px;
  padding-bottom: 24px;
`

const Header = styled.View`
  display: flex;
  flex-direction: row;
  padding-bottom: 12px;
`

const Title = styled.Text`
  ${{
    ...fonts.raleway.regular
  }}
  color: ${colors.primaryDark};
  text-align: left;
  font-size: 22px;
  line-height: 28px;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

const CloseBtn = styled.TouchableOpacity`
  width: 44px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-end;
`

const ActionsWrapper = styled.View`
  margin-left: -18px;
  margin-right: -18px;
  border-bottom-left-radius: 12px;
  border-bottom-right-radius: 12px;
  overflow: hidden;
  display: flex;
  flex-direction: row;
`

const ActionBtn = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
  underlayColor: colors.primaryDark
})`
  background-color: ${props => props.primary ? colors.primaryDark : colors.secondary};
  opacity: ${props => props.disabled ? 0.4 : 1};
  padding: 12px;
  flex: 1;
`

const ActionLabel = styled.Text`
  ${{ ...fonts.raleway.regular }};
  color: ${colors.white};
  font-size: 22px;
  text-align: center;
`

const ContentWrapper = styled(Content)`
  overflow: visible;
`

export default class Modal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      visible: this.props.visible
    }

    this.closeModal = this.closeModal.bind(this);
  }

  componentWillReceiveProps (newProps) {
    this.setState({
      visible: newProps.visible
    })
  }

  closeModal () {
    this.setState({
      visible: false
    })
    this.props.onRequestClose && this.props.onRequestClose()
  }

  render () {
    let action = this.props.action && this.props.action();
    return (
      <ReactModal
        animationType="fade"
        transparent={true}
        visible={this.state.visible}
        onRequestClose={this.props.onRequestClose}
      >
        <Overlay>
          <ContentWrapper>
            <Container>
              <Header>
                <Title>{this.props.title}</Title>
                <CloseBtn onPress={this.closeModal}>
                  <MaterialIcon name={'close'} color={colors.primaryDark} size={32} />
                </CloseBtn>
              </Header>
              <Body>
                {this.props.loading && (
                  <SpinnerLoader />
                )}
                {!this.props.loading && (this.props.children)}
              </Body>
              <ActionsWrapper>
                {!!action && (
                  <ActionBtn
                    disabled={action.disabled && action.disabled()}
                    primary={action.primary}
                    onPress={action.onPress}>
                    <ActionLabel>{action.label}</ActionLabel>
                  </ActionBtn>
                )}
                {!action && (
                  <ActionBtn primary={true} onPress={this.closeModal}>
                    <ActionLabel>fechar</ActionLabel>
                  </ActionBtn>
                )}
              </ActionsWrapper>
            </Container>
          </ContentWrapper>
        </Overlay>
        <Toast
          ref={c => {
            if (c && this.props.toast) this.props.toast.toastInstance = c;
          }}
        />
      </ReactModal>
    )
  }
}
