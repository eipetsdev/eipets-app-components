import React, { Component } from 'react';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { addNavigationHelpers } from 'react-navigation';
import { connect } from 'react-redux';
import { compose } from 'redux';

import composeWithAppContext from '../../../composers/composeWithAppContext';
import { getPetMonths, getPetYears } from '../../../helpers/pet-birthdate.helper';
import { getPetImageOrDefault } from '../../../helpers/pet-image.helper';
import HeaderAvatar from './HeaderAvatar';

class CurrentPetHeaderAvatar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      img: null,
      petYears: 0,
      petMonths: 0
    }

    this.bindImg = this.bindImg.bind(this);
    this.swapPet = this.swapPet.bind(this);
  }

  componentDidMount () {
    let currentPet = this.props.currentPet || {}
    this.bindImg(currentPet);
  }

  componentDidUpdate (prevProps) {
    let prevCurrentPet = prevProps.currentPet || {}
    let currentPet = this.props.currentPet || {}

    if (prevCurrentPet.checksum == currentPet.checksum)
      return;

    this.bindImg(currentPet);
  }

  bindImg (currentPet) {
    let petImg = getPetImageOrDefault(currentPet);
    let petYears = this.getPetYears(currentPet);
    let petMonths = this.getPetMonths(currentPet);

    this.setState({
      img: petImg,
      petYears,
      petMonths
    })
  }

  getPetYears (currentPet) {
    return getPetYears(currentPet);
  }

  getPetMonths (currentPet) {
    return getPetMonths(currentPet);
  }

  isFish (currentPet) {
    return currentPet.specieAlias === 'fish';
  }

  isShowPetYearsOld (currentPet) {
    return !!currentPet.birthdate && !this.isFish(currentPet);
  }

  swapPet () {
    this.props.navigation.navigate('SwapPet', {
      swapFilter: this.props.swapFilter
    });
  }

  render () {
    let currentPet = this.props.currentPet || {};
    return (
      !this.props.onlyAvatar
        ? (
          <HeaderAvatar
            navigation={this.props.navigation}
            bgImg={this.state.img}
            currentPet={true}
            mini={this.props.mini}
            hideOption={this.props.hideOption}
            swapFilter={this.props.swapFilter}
            title={this.props.title}
            smallTopMsg={'Olá, '}
            topMsg={currentPet.name}
            title={this.props.title}
            iconTitle={this.props.iconTitle}
            leftAddonText={this.isShowPetYearsOld(currentPet) ? this.state.petYears.toString() : false}
            leftAddonLabel={this.state.petYears == 1 ? 'Ano' : 'Anos'}
            rightAddonText={this.isShowPetYearsOld(currentPet) ? this.state.petMonths.toString() : false}
            onAvatarOptionPress={() => this.swapPet()}
            rightAddonLabel={this.state.petMonths == 1 ? 'Mês' : 'Meses'}>
            {this.props.children}
          </HeaderAvatar>
        )
        : (
          <HeaderAvatar
            navigation={this.props.navigation}
            avatar={this.state.img}
            currentPet={true}
            avatarName={currentPet.name}
            hideOption={this.props.hideOption}
            swapFilter={this.props.swapFilter}
            avatarOption={(props) => (<MaterialIcon {...props} name="swap-horiz" size={22} />)}
            onAvatarPress={() => this.swapPet()}
            onAvatarOptionPress={() => this.swapPet()}
            title={this.props.title}
            iconTitle={this.props.iconTitle}>
            {this.props.children}
          </HeaderAvatar>
        )
    )
  }
}
const mapStateToProps = (state) => ({
  nav: state.nav
})
const mapDispatchToProps = {}
const enhance = () => (
  compose(
    composeWithAppContext(),
    connect(mapStateToProps, mapDispatchToProps)
  )
);

export default enhance()(CurrentPetHeaderAvatar)