import React, { Component } from 'react'
import { Switch, Platform } from 'react-native'
import colors from '../config/colors';

export default class CustomSwitch extends Component {
  render () {
    return (
      <Switch
        onTintColor={colors.primary}
        thumbTintColor={Platform.select({ ios: null, android: colors.primaryDark })}
        {...this.props} />
    )
  }
}
