import React, { Component } from 'react';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { addNavigationHelpers } from 'react-navigation';
import { InteractionManager } from 'react-native'
import { connect } from 'react-redux';
import { compose } from 'redux';

import composeWithAppContext from '../../../composers/composeWithAppContext';
import { getPetImageOrDefault } from '../../../helpers/pet-image.helper';
import AvatarBox, { AvatarBoxOption } from './AvatarBox';

class CurrentPetAvatar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      img: null
    }

    this.getNavigator = this.getNavigator.bind(this);
    this.bindImg = this.bindImg.bind(this);
    this.swapPet = this.swapPet.bind(this);
  }

  getNavigator () {
    return addNavigationHelpers({ dispatch: this.props.dispatch, state: this.props.nav });
  }

  swapPet () {
    this.getNavigator().navigate('SwapPet', {
      swapFilter: this.props.swapFilter
    });
  }

  componentDidMount () {
    let currentPet = this.props.currentPet || {}
    this.bindImg(currentPet);
  }

  componentDidUpdate (prevProps) {
    let prevCurrentPet = prevProps.currentPet || {}
    let currentPet = this.props.currentPet || {}

    if (prevCurrentPet.checksum == currentPet.checksum)
      return;

    this.bindImg(currentPet);
  }

  bindImg (currentPet) {
    let petImg = getPetImageOrDefault(currentPet);
    this.setState({
      img: petImg
    })
  }

  render () {
    return (
      <AvatarBox
        style={{ ...this.props.style }}
        avatar={this.state.img}
        onAvatarPress={!this.props.hideOption && (() => this.swapPet())}
        onAvatarOptionPress={() => this.swapPet()}
        avatarOption={!this.props.hideOption && (() => (
          <AvatarBoxOption
            label={'Trocar pet'}
            icon={() => (<MaterialIcon name="swap-horiz" color={'#FFF'} size={22} />)} />
        ))} />
    )
  }
};

const mapStateToProps = (state) => ({
  nav: state.nav
});
const mapDispatchToProps = {};
const enhance = () => (
  compose(
    composeWithAppContext(),
    connect(mapStateToProps, mapDispatchToProps)
  )
);

export default enhance()(CurrentPetAvatar);