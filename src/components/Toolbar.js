import { Body, Header, Title } from 'native-base';
import React, { Component } from 'react';
import { Keyboard, TouchableHighlight, TextInput } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import fonts from '../assets/fonts';
import colors from '../config/colors';
import styled from 'styled-components/native';

const ToolbarBody = styled(Body)`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`

const StyledTextInput = styled.TextInput`
  flex: 1;
`

class Toolbar extends Component {
  constructor(props) {
    super(props);

    this.goBack = this.goBack.bind(this);
  }

  goBack () {
    if (!this.props.goBackAction)
      return;

    Keyboard.dismiss();
    this.props.goBackAction();
  }
  render () {
    return (
      <Header>
        <ToolbarBody>
          {this.props.showBackButton && (
            <TouchableHighlight
              onPress={this.goBack}
              underlayColor={colors.primary}
              activeOpacity={0.7}
              style={styles.iconWrapper}>
              <MaterialIcon name='arrow-back' size={26} color={colors.white} />
            </TouchableHighlight>
          )}
          <Title style={styles.title}>{this.props.title}</Title>
          {!!this.props.searchBar && (
            <TouchableHighlight
              onPress={() => { this.props.onSearch && this.props.onSearch() }}
              underlayColor={colors.primary}
              activeOpacity={0.7}
              style={styles.iconWrapper}>
              <MaterialIcon name='search' size={26} color={colors.white} />
            </TouchableHighlight>
          )}
          {this.props.showCloseButton && (
            <TouchableHighlight
              onPress={this.props.closeAction || this.goBack}
              underlayColor={colors.primary}
              activeOpacity={0.7}
              style={styles.iconWrapper}>
              <MaterialIcon name='close' size={26} color={'#FFF'} />
            </TouchableHighlight>
          )}
        </ToolbarBody>
      </Header>
    )
  }
}

export default Toolbar;

const styles = {
  iconWrapper: {
    flex: 0,
    marginLeft: -8,
    padding: 8
  },
  title: {
    ...fonts.raleway.regular,
    flex: 1
  }
}