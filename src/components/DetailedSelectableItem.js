import React, { Component } from 'react'
import styled from 'styled-components/native';
import { TouchableOpacity, Image, Dimensions } from 'react-native';
import { CachedImage } from 'react-native-cached-image';
import Checkbox from './Checkbox';
import colors from '../config/colors';
import fonts from '../assets/fonts';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import Lightbox from 'react-native-lightbox';

const screenWidth = Dimensions.get('screen').width;

const imageSizes = {
  width: screenWidth <= 360 ? 70 : 110,
  height: screenWidth <= 360 ? 70 : 110
}

const ListItem = styled.View`
  position: relative;
  flex-direction: column;
  padding-top: 24px;
  padding-bottom: 24px;
  border-bottom-width: 0.5px;
  border-bottom-color: ${colors.lightGray};
  background-color: ${colors.lightBackground};
  justify-content: center;
  align-items: center;
  height: ${props => props.height ? `${props.height}px` : 'auto'};
`

const Label = styled.Text`
  ${fonts.raleway.bold};
  color: ${colors.primaryDark};
  overflow: hidden;
  font-size: 17px;
  flex: 1;
`

const LabelWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`

const CheckboxWrapper = styled.View`
  width: 40px;
  justify-content: center;
  align-items: center;
`

const InfoWrapper = styled.View`
  flex: 1;
  padding-left: 12px;
  padding-right: 0;
`

const InfoText = styled.Text`
  ${fonts.raleway.regular};
  color: ${colors.textColor};
  font-size: 14px;
  font-style: ${props => props.italic ? 'italic' : 'normal'};
  line-height: 20px;
`

const ItemWrapper = styled.View`
  flex-direction: row;
`

const StyledCheckBox = styled(Checkbox)`
  margin-bottom: 0;
`

const StyledCachedImage = styled(CachedImage)`
  ${props => props.lightbox ? ({
    flexGrow: 1,
    height: props.height ? props.height : Dimensions.get('window').height
  }) : ({
    width: imageSizes.width,
    height: props.height ? props.height : imageSizes.height
  })}
  border-radius: ${props => props.lightbox ? 0 : 8}px;
  opacity: ${props => props.selected && !props.lightbox ? '0.5' : '1'};
`

const ImageWrapper = styled.View`
  position: relative;
  width: ${imageSizes.width + 6}px;
  height: ${imageSizes.height + 6}px;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  background-color: ${colors.primaryDark};
  border-width: ${props => props.selected ? '3px' : '0px'};
  border-color: ${colors.primaryDark};
`

const WarningsWrapper = styled.View`
  width: 100%;
`

const WarningItemWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: 8px;
`

const WarningText = styled.Text`
  ${fonts.raleway.regular};
  color: ${colors.primary};
  font-size: 14px;
  padding-left: 12px;
`

const Bold = styled.Text`
  font-weight: bold;
`

const StyledCheckIcon = styled(MaterialIcon)`
  position: absolute;
  top: 50%;
  left: 50%;
  margin-left: -18px;
  margin-top: -18px;
`

export default class DetailedSelectableItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      lightboxOpened: false
    }
  }

  render () {
    return (
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={this.props.onSelect}>
        <ListItem
          height={this.props.height}
        >
          <ItemWrapper>
            <ImageWrapper selected={this.props.selected}>
              <Lightbox
                willClose={() => { this.setState({ lightboxOpened: false }) }}
                onOpen={() => { this.setState({ lightboxOpened: true }) }}
              >
                <StyledCachedImage
                  source={this.props.image || {}}
                  lightbox={this.state.lightboxOpened}
                  resizeMode={this.state.lightboxOpened ? 'contain' : 'cover'}
                  height={this.state.lightboxOpened ? Dimensions.get('window').height : imageSizes.height}
                  selected={this.props.selected} />
              </Lightbox>
              {this.props.selected && (
                <StyledCheckIcon name={'done'} color={colors.white} size={38} />
              )}
            </ImageWrapper>
            <InfoWrapper>
              {!!this.props.title && (
                <LabelWrapper>
                  <Label numberOfLines={1}>{this.props.title}</Label>
                </LabelWrapper>
              )}
              {this.props.description && (
                <InfoText>{this.props.description}</InfoText>
              )}
              {this.props.customDescription}
              {this.props.infos && this.props.infos.map((info, index) => !!info.text && (
                <InfoText
                  key={index}
                  italic={info.italic}
                  numberOfLines={2}
                >
                  {!!info.title && (<Bold>{info.title}: </Bold>)}{info.text}
                </InfoText>
              ))}
            </InfoWrapper>
          </ItemWrapper>
        </ListItem>
      </TouchableOpacity >
    )
  }
}
