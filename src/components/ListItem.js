import React, {Component} from 'react'
import {Text, TouchableHighlight, View} from 'react-native'
import FeatherIcon from 'react-native-vector-icons/Feather';

import fonts from '../assets/fonts';
import colors from '../config/colors';

export default class ListItem extends Component {

  constructor(props) {
    super(props);

    this.getItemTemplate = this
      .getItemTemplate
      .bind(this);
  }

  getItemTemplate() {
    let text = this.props.text || '';
    return (
      <View
        style={{
        position: 'relative',
        padding: 16,
        flexDirection: 'row',
        alignItems: 'center'
      }}>
      {!this.props.children ? (
        <Text
          style={{
          flex: 1,
          fontSize: 17,
          ...fonts.raleway.regular,
          color: colors.textColor
        }}>{text}</Text>
      ) : this.props.children}
        <View>
          {(this.props.icon && (<this.props.icon style={styles.icon}/>)) || (this.props.onPress && (<FeatherIcon
            style={styles.icon}
            name="chevron-right"
            size={26}
            color={colors.textColor}/>))
}
        </View>
      </View>

    )
  }

  render() {
    return (
      <View
        style={{
        borderTopWidth: 1,
        borderColor: colors.lightGray
      }}>
        {this.props.onPress && (
          <TouchableHighlight
            onPress={(event) => !this.props.disabled && !!this.props.onPress && this.props.onPress(event)}
            onPressIn={() => {}}
            underlayColor={colors.lightBackground}
            activeOpacity={1}>
            {this.getItemTemplate()}
          </TouchableHighlight>
        )}
        {!this.props.onPress && (this.getItemTemplate())}
      </View>

    )
  }
}

const styles = {
  icon: {
    // position: 'absolute', right: 0
    // marginTop: -3
  }
}