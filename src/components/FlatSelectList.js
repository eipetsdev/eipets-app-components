import React, { Component } from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View
} from 'react-native';

import colors from '../config/colors';

const listStyles = StyleSheet.create({
  itemWrapper: {
    borderTopColor: colors.lightGray,
    marginTop: 6,
    paddingTop: 6
  }
});

class FlatItemComponent extends React.PureComponent {
  constructor(props) {
    super(props);

    this.getItemStyle = this.getItemStyle.bind(this);
    this.getItemTextStyle = this.getItemTextStyle.bind(this);
  }

  getItemStyle (item) {
    return {
      borderRadius: 12,
      borderWidth: 1,
      borderColor: 'transparent',
      overflow: 'hidden',
      backgroundColor: this.props.selected
        ? colors.primaryDark
        : 'transparent'
    }
  }

  getItemTextStyle (item) {
    return {
      padding: 12,
      fontFamily: 'Raleway',
      fontWeight: '500',
      fontSize: 18,
      color: this.props.selected
        ? '#FFF'
        : colors.primaryDark
    }
  }

  render () {
    const item = this.props.item;
    return (
      <View
        style={[
          {
            borderTopWidth: this.props.index == 0 ? 0 : 1
          },
          listStyles.itemWrapper
        ]}>
        <TouchableHighlight
          style={{
            borderRadius: 12,
            borderWidth: 1,
            borderColor: 'transparent'
          }}
          underlayColor={colors.primary}
          activeOpacity={0.7}
          onPress={() => this.props.onSelectItem(item)}>
          <View style={this.getItemStyle(item)}>
            <Text style={this.getItemTextStyle(item)}>{this.props.getItemLabel(item)}</Text>
          </View>
        </TouchableHighlight>
      </View>
    )
  }
}

export default class FlatSelectList extends Component {
  render () {
    return (
      <FlatList
        {...this.props}
        keyboardShouldPersistTaps={'always'}
        renderItem={({ item, index }) => (<FlatItemComponent
          item={item}
          index={index}
          selected={this.props.isSelectedItemExpression(item)}
          onSelectItem={this.props.onSelectItem}
          getItemLabel={this.props.getItemLabel} />)}
      />
    )
  }
}
