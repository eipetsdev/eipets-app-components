import {Textarea as NBTextarea} from 'native-base';
import React, {Component} from 'react'
import {View} from 'react-native';

import fonts from '../assets/fonts';
import colors from '../config/colors';

export default class Textarea extends Component {
  render() {
    return (
      <View
        style={[
        this.props.style, {
          borderWidth: 2,
          borderColor: colors.primary,
          borderRadius: 12,
          padding: 8,
          backgroundColor: colors.white
        }
      ]}>
        <NBTextarea
          {...this.props}
          style={[
          {
            height: 70,
            fontSize: 17,
            ...fonts.raleway.regular,
          }, {
            ...this.props.textareaStyle
          }
        ]}
          placeholderTextColor={colors.placeholder}/>
      </View>

    )
  }
}
