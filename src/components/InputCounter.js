import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import styled from 'styled-components/native';
import { colors, InputText } from '../../';

const Wrapper = styled.View`
  flex: 0.8;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

const ActionWrapper = styled.View`
  width: 42px;
  align-items: center;
  justify-content: center;
`

const InputWrapper = styled.View`
  flex: 1.5;
`

export default class InputCounter extends Component {
  constructor(props) {
    super(props);

    this.countUp = this.countUp.bind(this);
    this.countDown = this.countDown.bind(this);
  }


  countUp () {
    this.props.onChange && this.props.onChange(parseInt(this.props.value) + 1);
  }

  countDown () {
    let newCount = parseInt(this.props.value) - 1
    newCount = newCount <= 1 ? 1 : newCount;
    this.props.onChange && this.props.onChange(newCount);
  }

  render () {
    return (
      <Wrapper>
        <ActionWrapper>
          <TouchableOpacity onPress={this.countDown}>
            <MaterialIcon
              name={'remove'}
              size={30}
              color={colors.primaryDark}
            />
          </TouchableOpacity>
        </ActionWrapper>
        <InputWrapper>
          <InputText
            small
            value={this.props.value}
            style={{ textAlign: 'center', paddingLeft: 0, paddingRight: 0 }}
            keyboardType={'numeric'}
            onChangeText={(value) => this.props.onChange && this.props.onChange(value)} />
        </InputWrapper>
        <ActionWrapper>
          <TouchableOpacity onPress={this.countUp}>
            <MaterialIcon
              name={'add'}
              size={30}
              color={colors.primaryDark}
            />
          </TouchableOpacity>
        </ActionWrapper>
      </Wrapper>
    )
  }
}
