import React, {Component} from 'react'
import {StyleSheet, Text, View} from 'react-native'

const spacingForLetterIndex = (letters, index, spacing) => (letters.length - 1 === index)
  ? 0
  : spacing

class Letter extends Component {
  render() {
    const { children, spacing, textStyle } = this.props
    let letterStyles = [
      textStyle, {
        paddingRight: spacing
      }
    ]

    return (
      <Text style={letterStyles}>{children}</Text>
    )
  }
}

export default class TextWithLetterSpacing extends Component {
  render() {
    let {children, spacing, viewStyle, textStyle} = this.props
    let letters = children.split('')

    return (
      <View style={[styles.container, viewStyle]}>
        {letters.map((letter, index) => <Letter
          key={index}
          spacing={spacingForLetterIndex(letters, index, spacing)}
          textStyle={textStyle}>
          {letter}
        </Letter>)}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  }
})