import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

import fonts from '../assets/fonts';
import colors from '../config/colors';
import TextWithLetterSpacing from './TextWithLetterSpacing'

export default class SectionTitle extends Component {
  render () {
    let title = this.props.title || ''
    return (
      <View style={{
        ...styles.wrapper,
        ...this.props.wrapperStyle
      }}>
        <Text style={[styles.title, this.props.titleStyle]}>
          {title.toUpperCase()}
        </Text>
        {this.props.icon && (<this.props.icon style={styles.icon} size={50} />)}
        <View style={styles.line}></View>
      </View>
    )
  }
}

const styles = {
  wrapper: {
    position: 'relative',
    paddingTop: 36,
    backgroundColor: colors.primary
  },
  icon: {
    position: 'absolute',
    alignSelf: 'center',
    top: 28,
    opacity: 0.6
  },
  titleWrapper: {
    justifyContent: 'center'
  },
  title: {
    ...fonts.teko.semibold,
    textAlign: 'center',
    color: '#FFF',
    fontSize: 28,
    height: undefined,
    paddingLeft: 24,
    paddingRight: 24
  },
  line: {
    width: 1,
    height: 28,
    backgroundColor: '#FFF',
    alignSelf: 'center'
  }
}