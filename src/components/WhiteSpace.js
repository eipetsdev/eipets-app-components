import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

export default class WhiteSpace extends Component {
  render () {
    return Platform.select({
      ios: (
        <View style={styles.whiteSpace}></View>
      ), android: (
        <Text style={styles.whiteSpace}>  </Text>
      )
    })
  }
};

const styles = StyleSheet.create({
  whiteSpace: {
    width: 5,
    height: 1
  }
});