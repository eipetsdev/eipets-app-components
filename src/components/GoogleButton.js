import { Container, Text } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react'
import { Image, Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';

import fonts from '../assets/fonts';
import colors from '../config/colors';
import { CachedImage } from '../../../../node_modules/react-native-cached-image';

const shadown = {
  ...Platform.select({
    ios: {
      shadowColor: 'rgba(0,0,0, .3)',
      shadowOffset: {
        height: 2,
        width: 0
      },
      shadowOpacity: 10,
      shadowRadius: 2
    },
    android: {
      borderRadius: 2,
      elevation: 3
    }
  })
}

export default class GoogleButton extends Component {
  render () {
    return (
      <Container
        style={{
          flex: 0,
          height: 55,
          width: '100%',
          ...this.props.style
        }}>
        <TouchableOpacity
          onPress={this.props.onPress}
          onPressIn={() => { }}
          underlayColor={colors.lightBackground}
          activeOpacity={0.7}
          style={{
            backgroundColor: '#FFF',
            flex: 1,
            borderRadius: 8,
            flexDirection: 'row',
            alignItems: 'stretch',
            ...shadown
          }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              alignItems: 'center'
            }}>
            <View style={{
              paddingLeft: 14,
              paddingRight: 14,
              flex: 0
            }}>
              <CachedImage
                style={{
                  width: 28,
                  height: 28
                }}
                resizeMethod={'resize'}
                resizeMode={'contain'}
                source={require('../assets/images/google-logo.png')} />
            </View>
            <Text
              style={{
                flex: 1,
                color: colors.textColor
              }}>ENTRAR COM O GOOGLE</Text>
          </View>
        </TouchableOpacity>
      </Container>
    )
  }
}
