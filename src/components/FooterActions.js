import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import colors from '../config/colors';
import Card from './Card';
import IconAction from './IconAction';

export default class FooterActions extends Component {
  render () {
    return (
      <Card
        wrapperStyle={{
          position: 'relative',
          zIndex: 1,
          ...this.props.wrapperStyle
        }}
        style={{
          padding: 18,
          paddingTop: 24,
          paddingBottom: 24,
          backgroundColor: colors.primary,
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'center'
        }}>
        {!!this.props.customActions && (this.props.customActions)}
        {this.props.onDone && (
          <IconAction
            style={styles.action}
            onPress={this.props.onDone}
            color={colors.white}
            isActive={this.props.isDone}
            activeColor={colors.primary}
            underlayColor={colors.primary}
            label={'Feito'} />
        )}
        {this.props.onEdit && (
          <IconAction
            style={styles.action}
            onPress={this.props.onEdit}
            color={colors.white}
            underlayColor={colors.primary}
            label={'Editar'} />
        )}
        {this.props.onRemove && (
          <IconAction
            style={styles.action}
            onPress={this.props.onRemove}
            color={colors.white}
            underlayColor={colors.primary}
            label={'Excluir'} />
        )}
        {this.props.onRequest && (
          <IconAction
            style={styles.action}
            onPress={this.props.onRequest}
            color={colors.white}
            underlayColor={colors.primary}
            label={'Solicitar'} />
        )}
      </Card>
    )
  }
}

const styles = StyleSheet.create({
  action: {
    marginLeft: 18,
    marginRight: 18
  }
})