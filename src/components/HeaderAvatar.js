import { Body, Container, Header, Title } from 'native-base';
import React, { Component } from 'react'
import { Image, Platform, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { CachedImage } from 'react-native-cached-image';

import fonts from '../assets/fonts';
import colors from '../config/colors';
import AvatarBox from './AvatarBox';
import { AvatarBoxOption } from './AvatarBox';
import CurrentPetAvatar from './CurrentPetAvatar';
import SectionTitle from './SectionTitle';

function MiniHeader ({ props }) {
  return (
    <View style={{
      position: 'relative',
      height: props.bgImgHeight || 180,
      paddingBottom: 24
    }}>
      <View style={{
        height: (props.bgImgHeight || 180) - 44,
        position: 'relative',
        backgroundColor: colors.primary
      }}>
        {props.bgImg && props.bgImg.uri && (
          <CachedImage source={props.bgImg} style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: -12,
            right: -12,
            opacity: props.bgOpacity || 0.5,
            height: '100%',
          }} resizeMode={"cover"} />
        )}
        {props.bgImg && !props.bgImg.uri && (
          <CachedImage source={props.bgImg} style={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: -12,
            right: -12,
            opacity: 0.5,
            height: '100%',
          }} resizeMode={"cover"} />
        )}
      </View>

      <TouchableOpacity style={{
        position: 'absolute',
        backgroundColor: 'transparent',
        bottom: 0,
        left: 32,
        zIndex: 2,
        borderRadius: 55,
        borderWidth: 1,
        borderColor: colors.lightGray,
        overflow: 'hidden'
      }} activeOpacity={0.9} onPress={props.onAvatarOptionPress}>
        {props.bgImg && props.bgImg.uri && (
          <CachedImage source={props.bgImg} style={{
            opacity: 1,
            width: 110,
            height: 110,
            backgroundColor: colors.lightBackground
          }} resizeMode={"cover"} />
        )}
        {props.bgImg && !props.bgImg.uri && (
          <CachedImage source={props.bgImg} style={{
            opacity: 1,
            width: 110,
            height: 110,
            backgroundColor: colors.lightBackground
          }} resizeMode={"cover"} />
        )}
      </TouchableOpacity>
      <View style={{
        position: 'absolute',
        width: 150,
        height: 60,
        top: 65,
        left: 158,
        display: 'flex',
        justifyContent: 'center'
      }}>
        <Text style={{
          ...fonts.teko.semibold,
          color: colors.white,
          textAlignVertical: 'center',
          fontSize: 30,
          lineHeight: 36,
          opacity: 0.9
        }}>{props.title ? props.title.toUpperCase() : ''}</Text>
      </View>

      <View style={{
        position: 'relative',
        paddingBottom: 24,
        height: 35,
        left: 55,
        right: -12,
        zIndex: 1,
        backgroundColor: colors.primary
      }}>
        <Text style={{
          ...fonts.teko.regular,
          color: colors.white,
          fontSize: 26,
          lineHeight: 36,
          paddingLeft: 103
        }}>{props.topMsg && props.topMsg.length > 18 ? `${props.topMsg.substr(0, 18)}...` : props.topMsg}</Text>
      </View>
      <TouchableOpacity
        style={{
          position: 'absolute',
          left: 8,
          bottom: 14,
          zIndex: 3
        }}
        onPress={props.onAvatarOptionPress}>
        <AvatarBoxOption
          label={''}
          icon={() => (<MaterialIcon color={colors.primaryDark} name="swap-horiz" size={22} />)} />
      </TouchableOpacity>
    </View>
  )
}

function HeaderTitle (props) {
  return (
    <Container style={[styles.titleWrapper, {
      paddingTop: props.hideOption || !props.avatarName ? 48 : 75
    }]}>
      <SectionTitle
        title={props.title}
        icon={props.iconTitle}
      />
      {props.children && (
        <View style={{
          padding: 24
        }}>
          {props.children}
        </View>
      )}
    </Container>
  )
}

function HeaderBoxAddon ({ style, text, label }) {
  return (
    <Container
      style={[
        styles.boxAddonWrapper,
        { ...style }
      ]}>
      <Container style={styles.boxAddon}>
        <Text style={styles.addonText}>{text}</Text>
      </Container>
      <Text style={styles.addonLabel}>{label.toUpperCase()}</Text>
    </Container>
  )
}

function BigHeader ({ props }) {
  return (
    <View style={styles.wrapper}>
      <View style={styles.headerWrapper}>
        <Container style={[
          styles.backgroundWrapper,
          {
            backgroundColor: !!props.bgImg ? colors.lightBackground : colors.primary,
            height: !!props.bgImg ? (props.bgImgHeight || 170) : 110
          }
        ]}>
          {props.bgImg && (
            <CachedImage source={props.bgImg} style={[{
              height: props.bgImgHeight ? (props.bgImgHeight + 30) : 200,
              opacity: props.bgOpacity || 0.3,
              width: '100%'
            }]} resizeMode={"cover"} />
          )}
          <Container style={styles.welcomeMsgWrapper}>
            {!!props.smallTopMsg && (
              <Text
                style={[
                  {
                    fontSize: 18
                  },
                  styles.topMsg
                ]}>{props.smallTopMsg}</Text>
            )}
            {!!props.topMsg && (
              <Text
                style={[
                  {
                    fontSize: 22
                  },
                  styles.topMsg
                ]}>{props.topMsg}</Text>
            )}
          </Container>
        </Container>

        {props.currentPet ? (
          <CurrentPetAvatar
            navigation={props.navigation}
            hideOption={props.hideOption}
            swapFilter={props.swapFilter}
            style={{
              position: 'absolute',
              top: !!props.bgImg ? (props.bgImgHeight ? props.bgImgHeight - 80 : 90) : 18,
              zIndex: 2
            }} />
        ) : (<AvatarBox
          style={{
            position: 'absolute',
            top: !!props.bgImg ? (props.bgImgHeight ? props.bgImgHeight - 80 : 90) : 18,
            zIndex: 2
          }}
          avatar={props.avatar}
          onAvatarPress={props.onAvatarPress}
          onAvatarOptionPress={props.onAvatarOptionPress}
          avatarOption={props.avatarOption} />)}
        <View style={{
          position: 'absolute',
          width: '100%',
          top: props.avatarOption ? 170 : 150,
          alignItems: 'center',
          zIndex: 2
        }}>
          <Text style={{
            color: colors.white,
            ...fonts.raleway.regular,
            fontSize: 18
          }}>{props.avatarName}</Text>
        </View>
      </View>
      <HeaderTitle {...props} />
      {!!props.leftAddonText && (
        <HeaderBoxAddon style={{ left: 18 }} text={props.leftAddonText} label={props.leftAddonLabel} />
      )}
      {!!props.rightAddonText && (
        <HeaderBoxAddon style={{ right: 18 }} text={props.rightAddonText} label={props.rightAddonLabel} />
      )}
    </View>
  )
}

export default class HeaderAvatar extends Component {
  render () {
    if (this.props.mini) {
      return <MiniHeader props={{ ...this.props }} />
    } else {
      return <BigHeader props={{ ...this.props }} />
    }
  }
}

const shadown = {
  ...Platform.select({
    ios: {
      shadowColor: 'rgba(0,0,0, .3)',
      shadowOffset: {
        height: 0,
        width: 0
      },
      shadowOpacity: 10,
      shadowRadius: 10
    },
    android: {
      borderRadius: 10,
      elevation: 5
    }
  })
};

const styles = StyleSheet.create({
  titleWrapper: {
    position: 'relative',
    flex: 1,
    height: undefined,
    backgroundColor: colors.primary
  },
  wrapper: {
    flex: 0,
    height: undefined
  },
  headerWrapper: {
    ...Platform.select({
      ios: {
        position: 'relative',
        zIndex: 2
      }
    })
  },
  backgroundWrapper: {
    position: 'relative',
    overflow: 'hidden',
  },
  welcomeMsgWrapper: {
    position: 'absolute',
    top: 18,
    left: 18
  },
  topMsg: {
    ...fonts.raleway.regular,
    color: colors.primary
  },
  boxAddon: {
    ...shadown,
    flex: 0,
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    backgroundColor: colors.lightBackground
  },
  addonText: {
    ...fonts.teko.semibold,
    color: colors.primary,
    fontSize: 45,
    paddingTop: 8
  },
  addonLabel: {
    ...fonts.teko.semibold,
    width: 60,
    height: 60,
    color: '#FFF',
    marginTop: 6,
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 18
  },
  boxAddonWrapper: {
    position: 'absolute',
    top: 135,
    height: 100,
    zIndex: 2,
    padding: 4
  }
});