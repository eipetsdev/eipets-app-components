import React from 'react';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';

import config from '../assets/fonts/eipets-selection.json';
const Icon = createIconSetFromIcoMoon(config);

export default (props) => <Icon name={props.name} size={props.size} color={props.color || '#FFF'} style={props.style} />;