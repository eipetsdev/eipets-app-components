import AlphaScrollFlatList from 'alpha-scroll-flat-list';
import React, { Component } from 'react';
import { Platform, Text, View } from 'react-native';
import colors from '../config/colors';

class SectionHeader extends Component {
  render () {
    var textStyle = {
      textAlign: 'center',
      color: colors.white,
      fontWeight: '700',
      fontSize: 16
    };

    var viewStyle = {
      backgroundColor: colors.primary
    };
    return (
      <View style={viewStyle}>
        <Text style={textStyle}>{this.props.title}</Text>
      </View>
    );
  }
}

class SectionItem extends Component {
  render () {
    return (
      <Text style={{
        color: colors.primaryDark,
        paddingLeft: 3,
        paddingRight: 3
      }}>{this.props.title}</Text>
    );
  }
}

export default class AlphabeticFlatList extends Component {

  constructor(props) {
    super(props);
  }

  keyExtractor (item) {
    return item._id;
  }

  render () {
    return (
      <View style={{ flex: 1 }}>
        <AlphaScrollFlatList
          ref={this.props.ref}
          keyExtractor={this.keyExtractor.bind(this)}
          data={this.props.data}
          renderItem={this.props.cell.bind(this)}
          scrollBarStyleContainer={
            Platform.select({
              ios: {
                width: 36,
                right: -21
              },
              android: {
                width: 36,
                right: -12
              }
            })
          }
          scrollBarPointerContainerStyle={{
            right: 18
          }}
          activeColor={colors.primary}
          scrollBarColor={colors.primary}
          scrollBarFontSizeMultiplier={0.9}
          scrollKey={this.props.scrollKey}
          reverse={false}
          itemHeight={this.props.cellHeight}
        />
      </View>
    )
  }
}