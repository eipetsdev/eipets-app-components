import { Container, Text } from 'native-base';
import PropTypes from 'prop-types';
import React, { Component } from 'react'
import { Platform, StyleSheet, TouchableHighlight, View } from 'react-native';

import fonts from '../assets/fonts';
import colors from '../config/colors';

class PrimaryButton extends Component {
  constructor(props) {
    super(props);

    this.styles = StyleSheet.create(getStyles(props));
    this.getIcon = this.getIcon.bind(this);
  }

  componentWillReceiveProps (props) {
    this.styles = StyleSheet.create(getStyles(props));
  }

  getIcon () {
    return (
      <View style={this.styles.iconWrapper}>
        {this.props.icon}
      </View>
    )
  }

  render () {
    return (
      <Container style={this.styles.wrapper}>
        <TouchableHighlight
          onPress={(event) => !this.props.disabled && !!this.props.onPress && this.props.onPress(event)}
          onPressIn={() => { }}
          underlayColor="#000"
          activeOpacity={0.8}
          style={this.styles.touchWrap}>
          <View style={this.styles.container}>
            {this.props.icon && this.props.iconPosition !== 'right' && this.getIcon()}
            <View style={this.styles.button}>
              <Text style={this.styles.text}>{this.props.text}</Text>
            </View>
            {this.props.icon && this.props.iconPosition === 'right' && this.getIcon()}
          </View>
        </TouchableHighlight>
      </Container>
    )
  }
}

const sizes = {
  btnHeight: 55,
  smallBtnHeight: 42
};

PrimaryButton.sizes = sizes;

export default PrimaryButton;

const shadown = {
  ...Platform.select({
    ios: {
      shadowColor: 'rgba(0,0,0, .3)',
      shadowOffset: {
        height: 0,
        width: 0
      },
      shadowOpacity: 10,
      shadowRadius: 10
    },
    android: {
      borderRadius: 10,
      elevation: 5
    }
  })
}

const getStyles = (props) => {
  return {
    wrapper: {
      ...shadown,
      ...props.style,
      alignSelf: 'stretch',
      flex: 0,
      height: props.small ? sizes.smallBtnHeight : sizes.btnHeight,
      backgroundColor: 'transparent',
      borderColor: 'transparent',
      borderWidth: 0,
      opacity: props.disabled ? 0.3 : 1
    },
    container: {
      height: '100%',
      flexDirection: 'row'
    },
    touchWrap: {
      borderRadius: 10,
      flex: 0,
      alignSelf: 'stretch'
    },
    iconWrapper: {
      backgroundColor: colors.primaryDark,
      padding: 11,
      alignSelf: 'stretch',
      justifyContent: 'center',
      alignItems: 'center',
      width: 45,
      paddingLeft: 8,
      paddingRight: 8,
      borderRadius: 10,
      borderTopRightRadius: props.icon && props.iconPosition !== 'right' ? 0 : 10,
      borderBottomRightRadius: props.icon && props.iconPosition !== 'right' ? 0 : 10,
      borderTopLeftRadius: props.icon && props.iconPosition === 'right' ? 0 : 10,
      borderBottomLeftRadius: props.icon && props.iconPosition === 'right' ? 0 : 10
    },
    button: {
      flex: 1,
      alignSelf: 'stretch',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      backgroundColor: props.buttonColor || colors.primary,
      borderRadius: 10,
      borderTopRightRadius: props.icon && props.iconPosition === 'right' ? 0 : 10,
      borderTopLeftRadius: props.icon && props.iconPosition !== 'right' ? 0 : 10,
      borderBottomLeftRadius: props.icon && props.iconPosition !== 'right' ? 0 : 10,
      borderBottomRightRadius: props.icon && props.iconPosition === 'right' ? 0 : 10,
    },
    text: {
      ...fonts.raleway.bold,
      color: '#FFF',
      textAlign: 'center',
      fontSize: 14
    }
  }
};