import { H3 } from 'native-base';
import React, { Component } from 'react';

import fonts from '../assets/fonts';
import colors from '../config/colors';
import styled from 'styled-components/native';

const StyledHeader = styled(H3)`
  ${props => props.mode === 'bold' ? fonts.teko.semibold : fonts.raleway.regular};
  font-size: ${props => props.mode === 'bold' ? '28px' : '18px'};
  line-height: ${props => props.mode === 'bold' ? '38px' : '26px'};
  text-align: center;
  margin-bottom: ${props => props.doubleMargin ? '24px' : '12px'};
  color: ${colors.primary};
  ${props => props.style}
`

export default class SubHeader extends Component {
  render () {
    return (
      <StyledHeader {...this.props}>{this.props.children}</StyledHeader>
    )
  }
}
