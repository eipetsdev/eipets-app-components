export default {
  lightBackground: '#EFEFF4',
  lightGray: '#BCBCBC',
  extraLightGray: '#E1E1E1',
  darkGray: '#333333',
  primary: '#A02D71',
  primaryDark: '#4f0446',
  secondary: '#555556',
  textColor: '#333333',
  lightTextColor: '#8e8e8e',
  placeholder: '#595959',
  white: '#FFFFFF',
  green: '#55BA55',
  yellow: '#EDE628',
  red: '#ED3833',
  warning: '#FFD847',
  blue: '#61BEF9',
  orange: '#ff9d47',
  rgba: {
    primary: {
      solid: 'rgba(159, 21, 113, 1)',
      opacity: 'rgba(159, 21, 113, 0.4)'
    },
    primaryDark: {
      solid: 'rgba(79,4,70, 1)',
      opacity: 'rgba(79,4,70, 0.4)'
    },
    white: {
      solid: 'rgba(255,255,255, 1)',
      opacity: 'rgba(255,255,255, 0.4)'
    },
    red: {
      solid: 'rgba(237,56,51,1)',
      opacity: 'rgba(237,56,51,0.4)'
    },
    warning: {
      solid: 'rgba(255,216,71,1)',
      opacity: 'rgba(255,216,71,0.4)'
    },
    orange: {
      solid: 'rgba(255,157,71,1)',
      opacity: 'rgba(255,157,71,0.4)'
    },
    blue: {
      solid: 'rgba(97,190,249,1)',
      opacity: 'rgba(97,190,249,0.4)'
    },
    green: {
      solid: 'rgba(85,186,85,1)',
      opacity: 'rgba(85,186,85,0.4)'
    }
  }
}